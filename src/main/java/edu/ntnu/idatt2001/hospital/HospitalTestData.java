package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.healthpersonnel.Nurse;
import edu.ntnu.idatt2001.hospital.healthpersonnel.doctor.GeneralPractitioner;
import edu.ntnu.idatt2001.hospital.healthpersonnel.doctor.Surgeon;

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }

    /**
     * @param hospital tha is filled with data.
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployees(new Employee("Odd Even", "Primtallet", "78022288789"));
        emergency.addEmployees(new Employee("Huppasahn", "DelFinito", "10414526182"));
        emergency.addEmployees(new Employee("Rigmor", "Mortis", "78619593100"));
        emergency.addEmployees(new GeneralPractitioner("Inco", "Gnito", "37619728783"));
        emergency.addEmployees(new Surgeon("Inco", "Gnito", "25117919863"));
        emergency.addEmployees(new Nurse("Nina", "Teknologi", "45297008143"));
        emergency.addEmployees(new Nurse("Ove", "Ralt", "79785204441"));
        emergency.addPatients(new Patient("Inga", "Lykke", "58241277249"));
        emergency.addPatients(new Patient("Ulrik", "Smål", "32698401598"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployees(new Employee("Salti", "Kaffen", "39146386092"));
        childrenPolyclinic.addEmployees(new Employee("Nidel V.", "Elvefølger", "46192046240"));
        childrenPolyclinic.addEmployees(new Employee("Anton", "Nym", "28441003067"));
        childrenPolyclinic.addEmployees(new GeneralPractitioner("Gene", "Sis", "31627718172"));
        childrenPolyclinic.addEmployees(new Surgeon("Nanna", "Na", "08008336605"));
        childrenPolyclinic.addEmployees(new Nurse("Nora", "Toriet", "89094381731"));
        childrenPolyclinic.addPatients(new Patient("Hans", "Omvar", "56696983384"));
        childrenPolyclinic.addPatients(new Patient("Laila", "La", "75047365955"));
        childrenPolyclinic.addPatients(new Patient("Jøran", "Drebli", "09799164590"));
        hospital.addDepartment(childrenPolyclinic);
    }
}
