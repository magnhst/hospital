package edu.ntnu.idatt2001.hospital;

/**
 * Lets objects of the implementing class set its own diagnosis.
 * Is used in correlation with certain employees that is should
 * be able to set a patients diagnosis.
 * E.g A Surgeon sets a patients diagnosis with this method.
 */
public interface Diagnosable {

    /**
     * Sets the persons diagnosis
     * @param diagnosis the String diagnosis to give the person.
     */
    void setDiagnosis(String diagnosis);
}
