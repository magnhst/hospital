package edu.ntnu.idatt2001.hospital;

/**
 * Employee class extends Person class and represent the employees at the hospital.
 */
public class Employee extends Person{

    /**
     * Constructor. Creates a new Instance of Employee.
     * @param firstName String firstname of employee
     * @param lastName String lastname of employee
     * @param socialSecurityNumber String socialsecuritynumber of employee
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "edu.ntnu.idatt2001.hospital.Employee{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
