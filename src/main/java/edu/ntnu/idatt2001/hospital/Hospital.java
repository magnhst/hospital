package edu.ntnu.idatt2001.hospital;

import java.util.HashMap;

/**
 * Main Hospital class provides the main hospital which holds multiple departments.
 */
public class Hospital {
    private final String hospitalName;
    // Uses HashMap to easily find department object by name.
    private final HashMap<String, Department> departments = new HashMap<>();

    /**
     * Hospital constructor creates new instance of Hospital.
     * @param hospitalName is String name of hospital
     * @throws IllegalArgumentException if name is null or blank.
     */
    public Hospital(String hospitalName) {
        if (hospitalName == null || hospitalName.isBlank()){
            throw new IllegalArgumentException("hospitalName cannot be null or blank");
        }
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public HashMap<String, Department> getDepartments() {
        return new HashMap<>(departments);
    }

    /**
     * Adds a department to the hospital.
     * @param department is added to department register in the hospital.
     * @throws IllegalArgumentException if department is null or already exists in register
     */
    public void addDepartment(Department department){
        if (department == null) throw new IllegalArgumentException("Department cannot be null!");
        if (departments.containsValue(department)) {
            throw new IllegalArgumentException("SocialSecurityNumber already exists!");
        }
        departments.put(department.getDepartmentName(), department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
