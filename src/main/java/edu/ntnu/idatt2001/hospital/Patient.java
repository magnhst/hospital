package edu.ntnu.idatt2001.hospital;

/**
 * Patient class extends Person class and represent the patients at the hospital.
 * Includes the instance variable diagnosis which describes the how the patient
 * is sick.
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * Constructor. Creates a new Instance of Patient.
     * @param firstName String firstname of patient
     * @param lastName String lastname of patient
     * @param socialSecurityNumber String socialsecuritynumber of patient
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets new diagnosis for patient.
     * @param diagnosis is String diagnosis for patient.
     * @throws IllegalArgumentException if diagnosis is null or blank.
     */
    public void setDiagnosis(String diagnosis) {
        if (diagnosis == null || diagnosis.isBlank()) {
            throw new IllegalArgumentException("Diagnosis cannot be null or blank!");
        }
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "edu.ntnu.idatt2001.hospital.Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                ", firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
