package edu.ntnu.idatt2001.hospital;


import edu.ntnu.idatt2001.hospital.exception.RemoveException;

public class HospitalClient {

    public static void main(String[] args) {
        Hospital hospital = new Hospital("Bærum sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Demonstration of remove() removing a person from a department
        try {
            hospital.getDepartments().get("Akutten").remove("78022288789");
        } catch (RemoveException e){
            System.out.println(e.getMessage());
        }

        //Demonstration of remove() not finding person in register and throwing removeException
        try {
            hospital.getDepartments().get("Akutten").remove("00000000000");
        } catch (RemoveException e){
            System.out.println(e.getMessage());
        }
    }
}
