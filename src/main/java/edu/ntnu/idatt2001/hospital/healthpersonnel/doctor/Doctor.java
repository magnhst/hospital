package edu.ntnu.idatt2001.hospital.healthpersonnel.doctor;

import edu.ntnu.idatt2001.hospital.Employee;
import edu.ntnu.idatt2001.hospital.Patient;

/**
 * Doctor class extends Employee class and represent the doctors at the hospital.
 */
public abstract class Doctor extends Employee {
    /**
     * Constructor. Creates a new Instance of Doctor
     * @param firstName String firstname of doctor
     * @param lastName String lastname of doctor
     * @param socialSecurityNumber String socialsecuritynumber of doctor
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a patients diagnosis.
     * @param patient who's diagnosis is set.
     * @param diagnosis that is set.
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
