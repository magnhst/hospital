package edu.ntnu.idatt2001.hospital.healthpersonnel.doctor;

import edu.ntnu.idatt2001.hospital.Patient;

/**
 * Surgeon class extends Doctor class and represent the surgeons at the hospital.
 */
public class Surgeon extends Doctor{
    /**
     * Constructor. Creates a new Instance of Surgeon.
     * @param firstName String firstname of surgeon
     * @param lastName String lastname of surgeon
     * @param socialSecurityNumber String socialsecuritynumber of surgeon
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a patients diagnosis.
     * @param patient who's diagnosis is set.
     * @param diagnosis that is set.
     * @throws IllegalArgumentException if patient or diagnosis is null or diagnosis is blank.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        if (patient == null) throw new IllegalArgumentException("Patient cannot be null!");
        if (diagnosis == null || diagnosis.isBlank()) {
            throw new IllegalArgumentException("Diagnosis cannot be null or blank!");
        }
        patient.setDiagnosis(diagnosis);
    }
}
