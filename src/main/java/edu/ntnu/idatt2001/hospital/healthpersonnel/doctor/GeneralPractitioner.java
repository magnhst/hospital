package edu.ntnu.idatt2001.hospital.healthpersonnel.doctor;

import edu.ntnu.idatt2001.hospital.Patient;

/**
 * GeneralPractitioner class extends Doctor class and represent the general practitioners at the hospital.
 */
public class GeneralPractitioner extends Doctor{
    /**
     * Constructor. Creates a new Instance of GeneralPractitioner.
     * @param firstName String firstname of general practitioners
     * @param lastName String lastname of general practitioners
     * @param socialSecurityNumber String socialsecuritynumber of general practitioners
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a patients diagnosis.
     * @param patient who's diagnosis is set.
     * @param diagnosis that is set.
     * @throws IllegalArgumentException if patient or diagnosis is null or diagnosis is blank.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        if (patient == null) throw new IllegalArgumentException("Patient cannot be null!");
        if (diagnosis == null || diagnosis.isBlank()) {
            throw new IllegalArgumentException("Diagnosis cannot be null or blank!");
        }
        patient.setDiagnosis(diagnosis);
    }
}
