package edu.ntnu.idatt2001.hospital.healthpersonnel;

import edu.ntnu.idatt2001.hospital.Employee;

/**
 * Nurse class extends Employee class and represent the nurses at the hospital.
 */
public class Nurse extends Employee {
    /**
     * Constructor. Creates a new Instance of Nurse.
     * @param firstName String firstname of nurse
     * @param lastName String lastname of nurse
     * @param socialSecurityNumber String socialsecuritynumber of nurse
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "edu.ntnu.idatt2001.healthpersonnel.doctor.Nurse{" +
                "firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", socialSecurityNumber='" + getSocialSecurityNumber() + '\'' +
                '}';
    }
}
