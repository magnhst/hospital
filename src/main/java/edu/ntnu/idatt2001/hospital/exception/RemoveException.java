package edu.ntnu.idatt2001.hospital.exception;

/**
 * RemoveException class extends Exception which makes it a checked exception.
 * Should be thrown when we try to remove a Person from a collection that is
 * not present in the collection.
 */
public class RemoveException extends Exception {

    public RemoveException(){
        super("This person is not in any register.");
    }
}
