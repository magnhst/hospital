package edu.ntnu.idatt2001.hospital;


import edu.ntnu.idatt2001.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * Department class provides departments which holds employees and patients in the hospital.
 */
public class Department {
    private final String departmentName;
    // Uses HashMap to easily find Employee and Patient objects by their immutable socialSecurityNumber.
    private final HashMap<String, Employee> employees = new HashMap<>();
    private final HashMap<String, Patient> patients = new HashMap<>();

    /**
     * Constructor. Creates new instance of Department.
     * @param departmentName is a String name of the department.
     * @throws IllegalArgumentException if @param is null or blank
     */
    public Department(String departmentName) {
        if (departmentName == null || departmentName.isBlank()){
            throw new IllegalArgumentException("departmentName cannot be null or blank");
        }
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Creates a copy of the department with a new departmentName. Creates new instance of the
     * department with the new name and adds all employees and patients from old instance to
     * the new instance. This makes sure that the departmentName variable of the department
     * instance always matches its HashMap key.
     * @param departmentName the new department name to be set.
     * @throws IllegalArgumentException if @param is null or blank.
     * @return the copy of olde department with the new name.
     */
    public Department setDepartmentName(String departmentName) {
        if (departmentName == null || departmentName.isBlank()) {
            throw new IllegalArgumentException("Department name cannot be null or blank!");
        }
        Department newDepartment = new Department(departmentName);
        newDepartment.employees.putAll(employees);
        newDepartment.patients.putAll(patients);
        return newDepartment;
    }

    public HashMap<String, Employee> getEmployees() {
        return new HashMap<>(employees);
    }

    public HashMap<String, Patient> getPatients() {
        return new HashMap<>(patients);
    }

    /**
     * Adds an employee instance to departments employee register.
     * @param employee is added to employee register.
     * @throws IllegalArgumentException if @param is null or already exists in the employee
     * register.
     */
    public void addEmployees(Employee employee){
        if (employee == null) throw new IllegalArgumentException("SocialSecurityNumber or employee can not be null");
        if (employees.containsKey(employee.getSocialSecurityNumber())) {
            throw new IllegalArgumentException("SocialSecurityNumber already exists!");
        }
        employees.put(employee.getSocialSecurityNumber(), employee);
    }

    /**
     * Adds a patient instance to departments patient register.
     * @param patient is added to patient register.
     * @throws IllegalArgumentException if @param is null or already exists in the patient
     * register.
     */
    public void addPatients(Patient patient){
        if (patient == null) throw new IllegalArgumentException("SocialSecurityNumber or patient can not be null");
        if (patients.containsKey(patient.getSocialSecurityNumber())){
            throw new IllegalArgumentException("SocialSecurityNumber already exists!");
        }
        patients.put(patient.getSocialSecurityNumber(), patient);
    }

    /**
     * Removes person instance from its register. Person is abstract which means the method
     * can be used for both employee and patient instances.
     * @param socialSecurityNumber is used as key in register to locate specific person.
     *                             Indicates which person should be removed.
     * @throws IllegalArgumentException if @param is null or blank
     * @throws RemoveException if register cant find @param among the keys in the register.
     */
    public void remove(String socialSecurityNumber) throws RemoveException {
        if (socialSecurityNumber == null || socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("SocialSecurityNumber can not be null or blank!");
        }
        if (!employees.containsKey(socialSecurityNumber) && !patients.containsKey(socialSecurityNumber)){
            throw new RemoveException();
        }
        employees.remove(socialSecurityNumber); //HashMap method
        patients.remove(socialSecurityNumber);
    }

    /**
     * Removes person instance from its register. Person is abstract which means the method
     * can be used for both employee and patient instances.
     * @param person is used as value in register. Indicates which person should be removed.
     * @throws IllegalArgumentException if @param is null.
     * @throws RemoveException if register cant find @param among the values in the register.
     */
    public void remove(Person person) throws RemoveException {
        if (person == null) throw new IllegalArgumentException("Person can not be null!");
        if (!employees.containsValue(person) && !patients.containsValue(person)) {
            throw new RemoveException();
        }
        employees.remove(person.getSocialSecurityNumber()); //HashMap method
        patients.remove(person.getSocialSecurityNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) && employees.equals(that.employees) && patients.equals(that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}
