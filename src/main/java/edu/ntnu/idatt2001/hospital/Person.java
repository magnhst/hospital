package edu.ntnu.idatt2001.hospital;

import java.util.Objects;

/**
 * Person class is an abstract class containing methods that should be common
 * for it's subclasses.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private final String socialSecurityNumber;

    /**
     * Constructor. Creates a new Instance of Person
     * @param firstName String firstname of person
     * @param lastName String lastname of person
     * @param socialSecurityNumber String socialsecuritynumber of person
     * @throws IllegalArgumentException if firstname, lastname or socialsequritynumber
     * is null or blank.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if (firstName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("Firstname cannot be null or blank!");
        }
        if (lastName == null || lastName.isBlank()) {
            throw new IllegalArgumentException("Lastname cannot be null or blank!");
        }
        if (socialSecurityNumber == null || socialSecurityNumber.isBlank()) {
            throw new IllegalArgumentException("Socialsecuritynumber cannot be null or blank!");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets new name for firstname variable.
     * @param firstName is String firstname of person.
     * @throws IllegalArgumentException if firstname is null or blank.
     */
    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("Firstname cannot be null or blank!");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets new name for firstname variable.
     * @param lastName is String lastname of Instance.
     * @throws IllegalArgumentException if lastName is null or blank.
     */
    public void setLastName(String lastName) {
        if (lastName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("Lastname cannot be null or blank!");
        }
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Combines firstname and lastname into full-name and returns them in one string
     * @return full-name
     */
    public String getFullname(){
        return firstName+" "+lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        //Compares only socialsecuritynumber because we should not have any duplicates of this number among objects.
        return Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "edu.ntnu.idatt2001.hospital.Hospital{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
