package edu.ntnu.idatt2001.hospital;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersonTest {
    //Positive test
    @Test
    public void getFirstName_callsMethod_returnsFirstName(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertEquals("Ole",personDouble.getFirstName());
    }

    @Test
    public void setFirstName_stringArgument_setsStringAsNewFirstName(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        personDouble.setFirstName("Arne");
        assertEquals("Arne",personDouble.getFirstName());
    }

    @Test
    public void getLastName_callsMethod_returnsLastName(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertEquals("Martin",personDouble.getLastName());
    }

    @Test
    public void setLastName_stringArgument_setsStringAsNewLastName(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        personDouble.setLastName("Arne");
        assertEquals("Arne",personDouble.getLastName());
    }

    @Test
    public void getSocialSecurityNumber_callsMethod_returnsSocialSecurityNumber(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertEquals("24341414155",personDouble.getSocialSecurityNumber());
    }

    @Test
    public void getFullName_callsMethod_returnsFullName(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertEquals("Ole Martin",personDouble.getFullname());
    }


    //Negative test
    @Test
    public void PersonTest_firstArgumentIsNull_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            PersonDouble personDouble = new PersonDouble(null, "Martin", "24341414155");
        });
    }

    @Test
    public void PersonTest_secondArgumentIsNull_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            PersonDouble person = new PersonDouble("Ole",null,"24341414155");
        });
    }

    @Test
    public void PersonTest_thirdArgumentIsNull_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            PersonDouble person = new PersonDouble("Ole","Martin",null);
        });
    }

    @Test
    public void setFirstName_argumentIsNull_throwsIllegalArgumentException(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> personDouble.setFirstName(null));
    }

    @Test
    public void setLastName_argumentIsNull_throwsIllegalArgumentException(){
        PersonDouble personDouble = new PersonDouble("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> personDouble.setLastName(null));
    }
}
