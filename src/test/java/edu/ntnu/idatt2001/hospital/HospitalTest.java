package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.Department;
import edu.ntnu.idatt2001.hospital.Hospital;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class HospitalTest {

    //Positive test
    @Test
    public void getHospitalName_callsMethod_returnsHospitalName(){
        Hospital hospital = new Hospital("Bærum sykehus");
        assertEquals("Bærum sykehus",hospital.getHospitalName());
    }

    @Test
    public void getDepartments_callsMethod_returnsDepartments(){
        Hospital hospital = new Hospital("Bærum sykehus");
        Department emergency = new Department("Akutten");
        hospital.addDepartment(emergency);
        HashMap<String, Department> departments = new HashMap<>();
        departments.put("Akutten",emergency);
        assertEquals(departments,hospital.getDepartments());
    }

    @Test
    public void addDepartment_argumentIsValid_throwsIllegalArgumentException() {
        Hospital hospital = new Hospital("Bærum sykehus");
        Department emergency = new Department("Akutten");
        hospital.addDepartment(emergency);
        assertTrue(hospital.getDepartments().containsValue(emergency));
    }


    //Negative test
    @Test
    public void Hospital_argumentIsNull_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            Hospital hospital = new Hospital(null);
        });
    }

    @Test
    public void addDepartment_argumentIsNull_throwsIllegalArgumentException(){
        Hospital hospital = new Hospital("Bærum sykehus");
        Department emergency = new Department("Akutten");
        assertThrows(IllegalArgumentException.class, () -> hospital.addDepartment(null));
    }
}
