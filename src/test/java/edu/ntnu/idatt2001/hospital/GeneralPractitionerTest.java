package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.Patient;
import edu.ntnu.idatt2001.hospital.healthpersonnel.doctor.GeneralPractitioner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GeneralPractitionerTest {
    //Positive test
    @Test
    public void setDiagnosis_stringArgument_setsStringAsNewDiagnosis(){
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        generalPractitioner.setDiagnosis(patient,"Very sick");
        assertEquals("Very sick", patient.getDiagnosis());
    }

    //Negative test
    @Test
    public void setDiagnosis_firstArgumentIsNull_throwsIllegalArgumentException(){
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> generalPractitioner.setDiagnosis(null,"Very sick"));
    }

    @Test
    public void setDiagnosis_secondArgumentIsNull_throwsIllegalArgumentException(){
        GeneralPractitioner generalPractitioner = new GeneralPractitioner("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> generalPractitioner.setDiagnosis(patient,null));
    }
}
