package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.*;
import edu.ntnu.idatt2001.hospital.exception.RemoveException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;


public class DepartmentTest {

    //Positive test
    @Test
    public void getDepartmentName_callsMethod_returnsDepartmentName(){
        Department department = new Department("Akutten");
        assertEquals("Akutten", department.getDepartmentName());
    }

    @Test
    public void setDepartmentName_stringArgument_setsStringAsNewDepartmentName(){
        Department department = new Department("Akutten");
        department = department.setDepartmentName("Resepsjonen");
        assertEquals("Resepsjonen", department.getDepartmentName());
    }

    @Test
    public void getEmployees_callsMethod_returnsEmployees(){
        Department emergency = new Department("Akutten");
        Employee employee = new Employee("Ole", "Martin", "24341414155");
        emergency.addEmployees(employee);
        HashMap<String, Employee> employeeHashMap = new HashMap<>();
        employeeHashMap.put("24341414155", employee);
        assertEquals(employeeHashMap, emergency.getEmployees());
    }

    @Test
    public void getPatients_callsMethod_returnsPatients(){
        Department emergency = new Department("Akutten");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        emergency.addPatients(patient);
        HashMap<String, Patient> patientHashMap = new HashMap<>();
        patientHashMap.put("24341414155", patient);
        assertEquals(patientHashMap, emergency.getPatients());
    }

    @Test
    public void addEmployee_argumentIsValid_employeeIsAddedToDepartment() {
        Department emergency = new Department("Akutten");
        Employee employee = new Employee("Ole", "Martin", "24341414155");
        emergency.addEmployees(employee);
        assertTrue(emergency.getEmployees().containsValue(employee));
    }

    @Test
    public void addPatient_argumentIsValid_patientIsAddedToDepartment(){
        Department emergency = new Department("Akutten");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        emergency.addPatients(patient);
        assertTrue(emergency.getPatients().containsValue(patient));
    }

    //Negative test
    @Test
    public void Department_argumentIsNull_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            Hospital hospital = new Hospital(null);
        });
    }

    @Test
    public void addEmployee_argumentIsNull_throwsIllegalArgumentException(){
        Department emergency = new Department("Akutten");
        Employee employee = new Employee("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> emergency.addEmployees(null));
    }


    @Test
    public void addPatient_argumentIsNull_throwsIllegalArgumentException(){
        Department emergency = new Department("Akutten");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> emergency.addPatients(null));
    }


    @Nested
    class RemovePersonUsingPersonObject {
        // Positive tests
        @Test
        public void remove_patientIsInDepartment_patientIsRemoved() throws RemoveException {
            Department emergency = new Department("Akutten");
            Patient patient = new Patient("Ole", "Martin", "24341414155");
            emergency.addPatients(patient);
            emergency.remove(new Patient("Ole", "Martin", "24341414155"));
            assertFalse(emergency.getPatients().containsValue(patient));
        }

        @Test
        public void remove_employeeIsInDepartment_employeeIsRemoved() throws RemoveException {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            emergency.addEmployees(employee);
            emergency.remove(new Employee("Ole", "Martin", "24341414155"));
            assertFalse(emergency.getEmployees().containsValue(employee));
        }

        // Negative tests
        @Test
        public void remove_argumentIsNull_throwsIllegalArgumentException() {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(IllegalArgumentException.class, () -> emergency.remove((Person) null));
        }

        @Test
        public void remove_employeeIsNotInDepartment_throwsRemoveException() {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(RemoveException.class, () -> emergency.remove(employee));
        }

        @Test
        public void remove_patientIsNotInDepartment_throwsRemoveException() {
            Department emergency = new Department("Akutten");
            Patient patient = new Patient("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(RemoveException.class, () -> emergency.remove(patient));
        }
    }

    @Nested
    class RemovePersonUsingSocialSecurityNumber {
        // Positive tests
        @Test
        public void remove_patientIsInDepartment_patientIsRemoved() throws RemoveException {
            Department emergency = new Department("Akutten");
            Patient patient = new Patient("Ole", "Martin", "24341414155");
            emergency.addPatients(patient);
            emergency.remove(patient.getSocialSecurityNumber());
            assertFalse(emergency.getPatients().containsValue(patient));
        }

        @Test
        public void remove_employeeIsInDepartment_employeeIsRemoved() throws RemoveException {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            emergency.addEmployees(employee);
            emergency.remove(employee.getSocialSecurityNumber());
            assertFalse(emergency.getEmployees().containsValue(employee));
        }

        // Negative tests
        @Test
        public void remove_argumentIsNull_throwsIllegalArgumentException() {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(IllegalArgumentException.class, () -> emergency.remove((String) null));
        }

        @Test
        public void remove_employeeIsNotInDepartment_throwsRemoveException() {
            Department emergency = new Department("Akutten");
            Employee employee = new Employee("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(RemoveException.class, () -> emergency.remove(employee.getSocialSecurityNumber()));
        }

        @Test
        public void remove_patientIsNotInDepartment_throwsRemoveException() {
            Department emergency = new Department("Akutten");
            Patient patient = new Patient("Ole", "Martin", "24341414155");
            Exception exception = assertThrows(RemoveException.class, () -> emergency.remove(patient.getSocialSecurityNumber()));
        }
    }
}
