package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.Patient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PatientTest {
    //Positive test

    @Test
    public void setDiagnosis_stringArgument_setsStringAsNewDiagnosis(){
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        patient.setDiagnosis("Very sick");
        assertEquals("Very sick", patient.getDiagnosis());
    }

    //Negative test
    @Test
    public void setDiagnosis_argumentIsNull_throwsIllegalArgumentException(){
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> patient.setDiagnosis(null));
    }
}
