package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.Patient;
import edu.ntnu.idatt2001.hospital.healthpersonnel.doctor.Surgeon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SurgeonTest {
    //Positive test
    @Test
    public void setDiagnosis_stringArgument_setsStringAsNewDiagnosis(){
        Surgeon surgeon = new Surgeon("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        surgeon.setDiagnosis(patient,"Very sick");
        assertEquals("Very sick", patient.getDiagnosis());
    }

    //Negative test
    @Test
    public void setDiagnosis_firstArgumentIsNull_throwsIllegalArgumentException(){
        Surgeon surgeon = new Surgeon("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> surgeon.setDiagnosis(null,"Very sick"));
    }

    @Test
    public void setDiagnosis_secondArgumentIsNull_throwsIllegalArgumentException(){
        Surgeon surgeon = new Surgeon("Salti", "Kaffen", "39146386092");
        Patient patient = new Patient("Ole", "Martin", "24341414155");
        assertThrows(IllegalArgumentException.class, () -> surgeon.setDiagnosis(patient,null));
    }
}
