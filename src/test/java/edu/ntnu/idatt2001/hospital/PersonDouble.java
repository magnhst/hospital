package edu.ntnu.idatt2001.hospital;

import edu.ntnu.idatt2001.hospital.Person;

public class PersonDouble extends Person{
    /**
     * This class works as a extra person class which will only be used for testing.
     * Were testing this way so that the tests will apply for both Employee and Patient class,
     * because they both extend the abstract Person class and do not have any overridden methods
     * other than generic toString().
     *
     * This way we don't have to write employee or patient tests "twice".
     */
    public PersonDouble(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "PersonTest{}";
    }
}
